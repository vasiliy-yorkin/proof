import { compose, defaultProps } from 'recompose';

import { GridOwnProps, GridProps } from './types';
import Grid from './Grid';

export { getKey } from './utils';
export default compose<GridProps, GridOwnProps>(
  defaultProps({
    rowHeight: 40,
    columnWidth: 40,
    columnCount: 1000,
    rowCount: 1000,
    overscanColumnCount: 100,
    overscanRowCount: 100,
    fixedColumnCount: 1,
    fixedRowCount: 1,
  }),
)(Grid);
